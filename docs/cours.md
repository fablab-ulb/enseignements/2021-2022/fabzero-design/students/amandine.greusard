## **Design or not design ?**

Victor nous a présenté un cours de design, décomposant le processus de conception en 6 étapes. Nous avons également appris que les objets peuvent se classer selon deux catégories, les signes (la forme) et les outils (la fonction).

**1_ Déconstruction de la fonction de l'objet**

Avoir un regard détaché, tel un observateur curieux qui cherche à analyser et comprendre l'objet, son utilité, ses problèmes.

**2_ Résoudre un vrai problème**

Mettre en place des limites et des contraintes, ce sont elles qui font l'objet.

**3_ Comprendre les dimensions spécifiques**

Pensez en ergonome, au confort, au geste, au poids. Chaque objet à ses règles de dimensionnement.

**4_ Esquisse, essais-erreurs, améliorations**

Faire et refaire. Demander un avis extérieur. Prototypes sales - propres - fonctionnels

**5_ Adaptation, diffusion, édition**

Un objet unique étendu et reproduit. Adapté ou décliné pour pouvoir être diffusé au plus grand nombre. Il est réduit à sa plus simple expression avec un minimum de matière. Il est documenté et est accompagné d'un mode d'emploi.

**6_ Un nom, un pictogramme**

Un nom et un logo qui le rend lisible rapidement et facilement. Le met dans un contexte et le lie à un univers.

Durant ce cours de nombreux exemples ont été donné : Film de Jacques Tati - mon oncle, Venturi - Las Vegas street, les chaises de Peter Opsvik, Maarten Van Severen x Vitra, le fatboy, le vélo cowboy,...
