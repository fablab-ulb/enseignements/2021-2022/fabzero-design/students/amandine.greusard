## **Projet Final**

Ici, vous trouverez tout le processus de réflexion et création de mon objet.

## **Etape 1 _ Déconstruction de la fonction**

Les premières étapes consistaient à prendre du recul par rapport à l'objet. Par groupe de trois, nous avons cherché à déconstruire la fonction de notre objet. Avec un regard détaché, tel un anthropologue, un simple observateur ; afin de pouvoir analyser l'utilité première de cet objet.

Dans mon cas, le but est de pouvoir faire du café dans sa gourde personnelle. La fonction première est donc de **faire du café** qu'importe l'endroit. De plus, l'objet doit s'adapter aux caractéristiques de sa propre gourde. A partir de là, la simple fonction de faire du café peut être décomposée. Si on en prend tous les éléments, il faut avoir accès à du café moulu, de l'eau chaude et un filtre permettant à l'eau de passer au travers du café. L'objet est donc une valeur ajoutée à un outil déjà existant permettant au récipient initial d'être multi-fonctionnel.

## **Etape 2 _ Résoudre un vrai problème**

Toujours en groupe, nous nous sommes penchées sur le réel problème que soulevait cet objet. Ce qui permet de placer des limites, contraintes à la conception, elles détermineront l'objet.

Les enjeux de cet objet sont donc **l'adaptabilité** à son récipient, ici une gourde. Cette même gourde étant **transportable**, l'objet doit l'être aussi ce qui implique des contraintes de poids et d'encombrement.

Les objectifs sont donc de pouvoir faire du café, **son propre café selon ses goûts** partout en utilisant la gourde que l'on a déjà.


## Recherches

Pour partir sur de bonnes bases, je me suis renseignée sur les différentes façons de faire du café.

- Décoction : café moulu mélangé à de l'eau à température ambiante puis porté à ébullition

- Infusion : café moulu mélangé à de l'eau chaude, puis pressé ou non

- Percolation : passage de l'eau dans le café moulu par évaporation

- Cold brew : café moulu mélangé à de l'eau à température ambiante pendant plusieurs heures

- Lixiviation : passage de l'eau à travers le café moulu pour en extraire les substances solubles comme avec le café filtre

Dans le cadre de mon objet je me suis penchée sur les méthodes par **infusion** et **lixiviation** s'adaptant plus aux caractéristiques d'une gourde. J'ai également ajouté le cold brew à la liste pour les mêmes raisons.

En partant de cette idée, je me suis rendue dans une enseigne de torréfaction (Corica), afin de choisir le café le plus adapté à cette méthode. Il en est ressorti que le meilleur choix se tourne vers un café fruité car il bénéficie d'une torréfaction moins intense et libère plus d'arôme  contrairement à celui pour expresso où l'on recherche plus de corps.

Je me suis également renseignée sur le temps d'extraction nécessaire (temps passé entre la première et la dernière goutte versée). Dans le cas d'un café expresso avec une mouture fine il est de 20 à 30 secondes. Pour un café immergé ou filtré il se comprend entre 3 et 4 minutes. Dans le cas d'une méthode par french press l'extraction se fait par immersion, en utilisant un filtre elle se fait par gravitation.

Il existe différents types de filtre à café. Les plus commercialisés sont en papier à usage unique. Mais on trouve également des filtres en métal ou en tissus. Les filtres en métal on un maillage plus large que ceux en papier ce qui laisse d'avantage passer les huiles contenues dans les grains de café. Il en résulte un café plus riche, plus plein, plus sucré et moins acide, ce qui ce ressent notamment dans la texture en bouche.

## Echantillonnage

Afin d'évaluer comment remplir la fonction première de mon objet, j'ai fait plusieurs tests pour trouver quel café fait de façon simple était le plus qualitatif. (Ces tests sont dirigés selon un point de vue personnel.)

![](./images/review1.png)

J'ai effectué un échantillonnage à l'aide d'une french press en respectant les mêmes dosages, températures et temps d'infusion. (2 cuillères à café pour 15 cl d'eau infusé pendant 4 min avec de l'eau à environ 90 °C)

![](./images/coffeereview.jpg)

Après ces tests, j'ai conclu sans grande surprise que le café fait de manière traditionnelle avec la french press, donc pressé et filtré était le meilleur, cependant lors d'un deuxième échantillonnage en augmentant la dose de café utilisé passant de 2 càc à 3 càc pour 15 cl d'eau, le résultat était plus que convenant lors d'une simple filtration sans pressage.

**Deuxième phase de test**

J'ai effectué d'autres tests avec des filtres en métal en papier et sans filtre, dans trois contenants différents, une gourde, une thermos et une tasse.

Dans tout les cas la température de l'eau est montée à 80-90°C et le temps d'infusion est de 4 min, à l'exception du premier test où le filtre n'est pas immergé et le temps d'extraction est de 3 min.

1_ Tasse + filtre métallique

2_ Tasse + filtre métallique immergé

3_ Thermos + filtre papier, en versant l'eau à l'extérieur du filtre

4_ Thermos + filtre papier, en versant l'eau à l'intérieur du filtre

5_ Thermos + filtre papier, en versant l'eau avant le café

6_ Gourde sans filtre

7_ Gourde sans filtre, en versant l'eau avant le café

Conclusion, le filtre métallique apporte une texture très agréable au café. Verser l'eau avant le café permet dans certains cas de préparation de café filtre de diminuer le temps d'extraction de moitié, ici, cela inhibe les arômes.

## **Etape 3 _ Comprendre les dimensions spécifiques**

Cet objet doit pouvoir s'adapter à ma gourde personnelle, j'en ai donc pris les dimensions.

![](./images/fpdim.png)

![](./images/fpdim2.jpg)

Le bouchon en lui-même correspond aux dimensions Ø 5,5 cm x 4 cm, poids : 24 grammes.

Afin de dimensionner le filtre, j'ai d'abord chercher à savoir quelle quantité de café me sera nécessaire par rapport au volume d'eau de ma gourde (0.8 L). D'après les mesures de mon test, j'ai établi que j'avais besoin de 3 cuillères à café de café moulu pour 15 cl d'eau. Pour la contenance de ma gourde, il m'en faut donc 5 fois plus, soit 15 cuillères à café ou 5 cuillères à soupe pour 0.8 L d'eau. Ces 15 cuillères à café sont environ équivalentes à 75-80 grammes de café moulu.

Mes recherches m'ont appris que la taille du filtre devait permettre qu'il puisse presque toucher le fond de la gourde afin de permettre une plus grande surface de contact avec l'eau. Ainsi, en calculant le volume du filtre pour le faire correspondre avec la quantité de café, j'ai calculé que selon un diamètre de 25 mm la hauteur de mon armature devait correspondre à 163 mm.

Tous ces calculs sont un absolu puisque le filtre ainsi que le café moulu rajoute du volume dans la gourde ainsi le volume d'eau maximum sera légèrement moins important. Il en est de même pour la taille du filtre calculée pour un volume maximum de café permettant une certaine marge quant aux dosages.

En ce qui concerne le maillage du filtre à café, je me suis renseignée sur les filtres classiques en papier que je n'utiliserais pas ici, car un système réutilisable et lavable est nécessaire. Je me suis donc tournée vers les filtres en tissus (nylon) et en métal (acier inoxydable). Ceux-ci correspondant à une taille de mesh (maillage) précis afin d'être adapté au café moulu. Ainsi, je me réfère à un filtre métal de mesh 500, correspondant à 25 microns. En comparaison, un filtre en papier standard se rapproche de 20 microns, soit un maillage de 635.

## **Etape 4 _ Esquisses, essais-erreurs, améliorations**

**Essais de dimensionnement**

J'ai commencé par des essais d'impression suite aux dimensionnements, notamment au niveau des filetages dans le but de tester un bouchon pouvant être visé à ma gourde ainsi qu'avec un système de filtre à café.

![](./images/fpb1.png)

Les problèmes rencontrés lors de ce test sont le calibrage du filetage trop étroit pour permettre un emboîtement. Par manque de temps, j'ai dû réduire l'échelle des pièces pour permettre un temps d'impression plus court, ce qui fait que je n'ai pas pu tester de visser le bouchon sur ma gourde.

Suite à ces problèmes, j'ai effectué des modifications puis procédé à des tests de filetage à échelle 1.

![](./images/finalproject/filetage.png)

## Idée 1

Ma première idée pour cet objet était de me rapprocher du système de french press, en utilisant un poussoir ou en créant de la pression. Cette réflexion a persisté pendant un long moment, mais tous les schémas et croquis aboutissaient à de multiples complications. Comme le fait que toutes les gourdes ne sont pas parfaitement circulaires et par conséquent qu'un filtre intérieur serait en peine de pouvoir longer les parois sans laisser échapper des particules de café. Ou encore comment intégrer un système de poussoir par la seule entrée prodiguée par une gourde qui servirait également à boire. Après plusieurs discussions avec différentes personnes, j'ai choisi de m'éloigner de ce système amenant plus de problèmes que de solutions. De plus, l'échantillonnage réalisé plus haut a montré que la fonction de press n'est pas indispensable après un redosage de café.

![](./images/finalproject/press.png)

1_ Press à piston selon un exemple de french press et dans une gourdes
2_ Press à piston dans un filtre de diamètre défini
3_ Filtre poussoir à travers la paroi nécessitant un système d'étanchéité
4_ Exemple de cafetière nomade de la marque Wacaco utilisant la pression

## Idée 2

En parallèle de cette idée, je me suis laissée à penser à ce que pourrait donner un système d'aimants pour filtrer et faire descendre mécaniquement le café moulu dans le fond de la gourde. Avec un aimant situé sur la paroi extérieure et un filtre aimanté à l'intérieur. Les soucis auxquels je peux très vite penser sont similaires à ceux de la première idée de poussoir, avec un filtre non adapté à toutes les courbures des différentes gourdes. Ou encore le fait d'avoir un aimant en permanence sur sa gourde qui se situe en général dans un sac, résultant à l'accrochage de tout objet métalliques.

![](./images/finalproject/magnet.jpg)

## Idée 3

Une autre idée était de pouvoir dévisser le bas de la gourde afin d'y créer un récipient pour le café moulu. Cependant l'idée même de ce projet et de pouvoir s'adapter à toutes les gourdes en l'état, hors ici cette manipulation engendrerait une modification importante de l'objet et ne serait pas réalisable par tous.

![](./images/finalproject/devisser.jpg)

## Idée 4

En reprenant le principe même d'infusion, j'ai pensé à utiliser un filtre souple (nylon) comme principal outil. Mais dans ce cas, j'avais l'impression de ne pas pousser le projet assez loin sachant le nombre de possibilités offertes par les outils disponibles au fablab.

![](./images/finalproject/nylon.jpg)

## Prototype 1

J'ai ensuite cherché à dessiner une armature pour ce filtre sur Fusion360 s'adaptant à la taille de ma gourde.

![](./images/fpessais2.png)

Il m'a fallu deux impressions pour arriver à un résultat convaincant.

![](./images/finalproject/prototype.png)

Ici, l'idée de pouvoir combiner cette armature avec un filtre métallique de mesh 500, soit assez fin pour laisser passer l'eau, mais pas le café moulu, tel que celui-là :

![](./images/fpfiltremesh500.png)

En ce qui concerne le système d'accroche entre le filtre et son armature, j'ai envisagé de le coincer entre les parois de l'armature par des clips ou en le glissant par une des extrémités de l'armature, par exemple avec un fond dévissable.

Après avoir obtenu ce prototype je me suis de nouveau rendue à Corica pour avoir leurs avis. Ils m'ont donné des pistes d'améliorations, telles que : 

- La création d'un boîtier de rangement pour le filtre, puisque contrairement au thé ou autres infusions, il est préférable de retirer le café de l'eau chaude après la diffusion de ses arômes. 

- Le découpage du fond du filtre pour permettre un meilleur lavage.

- Des tests sur des gourdes thermos, en établissant par exemple un catalogue des gourdes les plus populaires avec leurs adaptations de l'objet selon leurs caractéristiques.

Ils m'ont également donné des conseils sur la préparation du café avec l'objet, comme effectuer des essais de différentes moutures et se permettre d'utiliser une eau montée à plus haute température car la perte de chaleur est plus importante dans une gourde, sans pour autant monter en ébullition.

## Prototype 2

Après avoir constaté la déformation du PLA dans l'eau chaude, j'ai effectué des tests en PET(G) (Polyéthylène Téréphtalate (Glycol)), qui est une des matières utilisées pour les emballages alimentaires. Cette matière ne se déforme pas dans l'eau chaude mais peut se ramolir dans l'eau bouillante.

Afin de simplifier et de rendre encore plus adaptable l'objet, je l'ai détaché du bouchon, en faisant un objet en soi. Sa base permet de le reposer sur tout types d'ouvertures le temps de l'infusion.

En le dessinant en deux parties cela permet d'y incorporer le filtre métallique dans son épaisseur. Afin de définir les meilleures dimensions de l'emboîtement de ces deux pièces j'ai imprimer plusieurs tests suivant un rétrécissement de -0.2mm, -0.5mm, et -1mm. Le rétrécissement le plus adapté est celui de -0.5mm.

![](./images/finalproject/emboitement.jpg)

Après impression, les deux pièces s'emboîtent parfaitement.

![](./images/finalproject/protofndebout.jpg)

![](./images/finalproject/protofn.jpg)

![](./images/finalproject/protogourde.jpg)

## **Etape 5 _ Adaptation, Diffusion, Edition**

Mode d'emploi :

Remplir le filtre de café moulu

Placer le filtre dans la gourde à vide

Verser de l'eau chaude (entre 80 et 90°C) dans le filtre

Laisser infuser pendant 4 minutes

Retirer le filtre

Déguster votre boisson

## **Liens Utiles**

[Fichier .stl filtre](./images/finalproject/finalprint_filtre05.stl)

[Fichier .stl boîte de rangement avec sa tasse](./images/finalproject/finalprint_etui_tasse.stl)

[Fichier .stl du bouchon avec son filtre](./images/finalproject/bouchonetfiltre.stl)

[Fichier .stl du filetage du filtre](./images/finalproject/filetagefiltre.stl)
