## A PROPOS DE MOI

![](images/moi.jpg)

Hello!
Je m'appelle Amandine Greusard, actuellement étudiante en première année de **master en architecture**.


## Mon parcours

Je suis née à Belfort, une petite ville de l'est de la France. Après l'obtention du baccalauréat j'ai suivi une préparation aux arts plastiques et à l'architecture pendant un an à Besançon. Puis j'ai entamé ma licence d'architecture à Montpellier, durant laquelle je suis partie en Erasmus en République Tchèque. Et enfin, je suis venue m'installer à Bruxelles pour effectuer mes deux ans de master à L'ULB.


### Mon objet

Pour ce cours il nous a été demandé de choisir un objet proche de nous afin de le repenser. Etant de nature très pragmatique j'ai voulu selectionner un objet dont j'ai besoin et qui me servira tous les jours. Mon choix s'est porté sur ma gourde, à laquelle j'aimerai mêler la fonction de cafetière, selon le principe d'une french press.

![](images/gourde.jpg)
