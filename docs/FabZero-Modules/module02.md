# 2. Conception Assistée par Ordinateur

Cette semaine nous avons travaillé à l'élaboration du dessin de notre objet à l'aide du logiciel Fusion 360.

## Logiciel Fusion 360

## 1. Exemple d'utilisation du logiciel

Afin de se familiariser avec ce nouvel outil, nous avons dessiné le cube du FabLab.

Dans un premier temps nous avons utilisé l'outil _esquisse_ pour dessiner un carré de 100x100mm, grâce à la commande     _rectangle en partant du centre_, afin d'être positionné sur l'origine de la grille de dessin.

![](../images/cube1.png)

Avec l'outil _extrusion symétrique_ nous avons tranformé l'esquisse en un cube.

![](../images/cube2.png)

De nouveau dans l'outil _esquisse_, on vient dessiner un _cercle à partir du centre_ de 50mm, en se positionnant toujours sur l'origine de la feuille de dessin.

![](../images/cube3.png) 

On _extude_ ensuite ce cercle afin de percer la totalité du cube.
![](../images/cube4.png)

En utilisant l'outil _réseau circulaire_ en selectionnant le type _features_ ce qui va permettre de copier la commande de l'objet selectionné. On vient donc selectionner la percée précedément crée ainsi que l'axe bleu. En selectionnant un angle de 90° on va faire pivoter l'extrusion cylindrique pour créer une deuxième percée.

![](../images/cube5.png)

On va ensuite répéter cette action afin de dessiner une extrusion perpendiculaire aux précédentes. Pour selectionner le bon axe il suffit de cliquer sur la permière extusion de la figure.

![](../images/cube7.png)

De nouveau dans l'outil _esquisse_, on dessine un rectangle dont une des largeurs de 25mm est centrée sur l'origine de la feuille de dessin. Afin de vérouiller la figure on entre ses _dimensions_ avec le raccourci clavier D.

![](../images/cube8.png)

Avec le raccourci clavier E, on _extrude_ cette nouvelle partie.

![](../images/cube9.png)

Dans le défilant _construction_ on choisit la commande _axe par deux points_, pour dessiner la diagonale du cube passant par les deux extrémités de par et d'autre de l'extusion précédente.

![](../images/cube10.png)

Avec l'outil _réseau rectangulaire_, on sélectione notre extusion ainsi que l'axe. Pour créer deux nouvelles extrusions selon une rotation complète autour de l'axe.

![](../images/cube11.png)

La dernière étape consiste à _conger_ les arrêtes de notre cube. Pour ce faire, on vient déplacer le curseur de l'historique de commande situé en bas à gauche jusqu'au moment ou nous avions un simple cube. Que l'on va sélectionner puis donner une _conger_ de 10mm.

![](../images/cube12.png)

En revenant à la dernière commande de l'historique vous pouvez voir votre cube terminé!

![](../images/cube13.png)

## 2. Dessin de notre objet

Cet exercice nous permet de mieux connaître le logiciel afin de pouvoir dessiner notre objet, ici une gourde.

Dans l'_esquisse_ j'ai dessiné le profil de mon objet à l'aide des outils _lignes_, _arc en trois points_ et _décaler_

![](../images/gourde1.png)

Ensuite avec _révolution_ et en sélectionnant mon esquisse et l'axe bleu, j'ai obtenu un premier modèle de mon objet.

![](../images/gourde2.png)

J'ai ajouté un filetage de 62mm avec l'outil _thread_.

![](../images/gourde3.png)

J'ai ajouter un _paramètre_, qui nous permettra d'avoir des fonctions variables accorder à notre objet, ici son épaisseur.

![](../images/parametre.png)

_Pour télécharger le fichier .stl [Cliquez ici](../images/objetfabgourdeoriginal.stl)_
