# 1. Documentation

Cette semaine nous avons travaillé à la mise en place des outils numériques qui seront utilisés tout au long du quadrimestre. Pour ce faire nous avons débuté avec une première apporche de GitLab, suivi de son installation.

## Installation et Configuartion Git 

**1. Utilisation du terminal bash sous Windows**

Dans un premier temps, j'ai du accéder au terminal bash. Pour ce faire :

Panneau de configuration -> Programmes -> Programmes et fonctionnalités -> Activer ou désactiver des fonctionnalités Windows

Il ne reste plus qu'à activer :

Sous-système Windows pour Linux

Ceci fait, il me restait à télécharger Ubuntu.

**2. Installation et Configuration Git**

Depuis le terminal vous pouvez verifier si Git est déja installé sur votre ordinateur comme c'était mon cas avec la commande : `git --version`

Ce qui vous donnera la version Git déjà installé.

La prochaine étape consiste à configurer Git. Il suffit d'entrer, toujours dans le terminal, vos identifiants avec ces commandes :

`git config --global user.name "your_username"`

`git config --global user.email "your_email_address@example.com"`

**3. Créer une clé SSH**

Il vous sera ensuite demandé de créer une clé SSH. Afin de vérifier quelle version OpenSSH vous possédez saisissez la commande : `ssh -V`

Avant de créer une clé, vérifiez qu'il n'en existe pas déjà avec la commande : `.ssh/`

![](../images/screen1module1.png)

Enfin pour générer une clé SSH saisissez la commande suivante en remplaçant "comment" pour vous permettre de l'identifier, dans mon cas j'ai entré mon adresse e-mail : `ssh-keygen -t ed25519 -C "<comment>"`

Une fois la clé générée il vous est demandé d'entrer un mot de passe "passphrase", cependant en cliquant sur entrée vous pouvez continuer sans, et récupérer votre clé nouvellement crée.

![](../images/screen2module1.png)

La prochaine étape est de copier cette clé afin de l'ajouter à votre compte GitLab, en utilisant la commande (sous Windows) :

`cat ~/.ssh/id_ed25519.pub`

![](../images/screen3module1.png)

Vous pouvez ensuite copier cette clé sur votre compte GitLab en ligne : User settings -> Preferences -> SSH Keys

**4. Cloner le projet via la clé SSH**

Cloner votre projet vous permet de travailler sur GitLab depuis votre ordinateur. Pour ce faire, il vous faut choisir l'emplacement du dossier de votre projet et le rentrer dans le terminal (commande cd "change directory", ls "afficher les dossiers").

Sur votre espace GitLab vous trouverez l'onglet Clone dans lequel vous pourrez copier l'adresse Clone with SSH.

![](../images/screen4module1.png)

Pour ensuite dans le terminal entrer la commande : `git clone` suivi de l'adresse précédemment copier.

Il ne reste plus qu'à entrer "yes" pour confirmer le clonage.

![](../images/screen5module1.png)
