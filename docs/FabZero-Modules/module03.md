# 3. Impression 3D

Cette semaine l'exercice consistait à prendre un main les imprimantes 3D disponible au Fablab. Pour cela nous avons imprimé un modèle réduit ou une partie de notre objet réalisé sur Fusion 360 la semaine précédente, afin de ne pas dépasser un temps d'impression de plus de deux heures.

## Paramétrage

Les imprimantes à notre disposition au Fablab sont des Prusa i3 MK3, i3 MK3S. Le logiciel associé sur lequel les paramétrages vont s'effectuer s'appelle PrusaSlicer.

Sur ce logiciel on peut venir importer notre objet dessiné sur Fusion360 après l'avoir exporté au format .stl. Pour répondre à la contrainte des dimensions et du temps d'impression, j'ai modifié mon objet sur Fusion à l'aide de l'outil _échelle_. Cette manipulation peut également se faire depuis le logiciel PrusaSlicer.

Une fois le fichier .stl importer sur PrusaSlicer, on peut commencer les réglages de l'impression. Pour avoir accès à tout les paramètres j'ai selectionné le _mode expert_ disponible en haut à droite. Sur ce menu vous pouvez aussi sélectionné le modèle de l'imprimante ainsi que le filament utilisé. 

![](../images/01m3.png)

Dans les réglages d'impression, je viens choisir les paramètres appropriés pour ce petit objet test.

Dans _couches et périmètres_ je règle sur deux couches en vertical et horizontal.
Dans _remplissage_ je peux choisir la densité de remplissage de l'objet afin d'améliorer sa solidité. Ici l'objet est creux, le remplissage n'influera pas sur le temps d'impression.
Dans _supports_ je viens créer un radeau de 3 couches, l'objet échelle 1:1 de par son élévation nécessiterait un radeau plus important voire même des supports pour le maintenir lors de l'impression.

![](../images/02m3.png).![](../images/03m3.png).![](../images/04m3.png)

Enfin sur le menu _plateau_ j'ai pu avoir une estimation du temps d'impression en cliquant sur _découper maintenant_. En exportant le G-code vous récupérez le fichier lisible pour l'imprimante à placer sur sa carte SD.

![](../images/05m3.png)

## Impression

Avant de lancer l'impression, on doit vérifier que le plateau est propre (si non le nettoyer avec quelques gouttes d'acétone), que la quantité de filament sur la bobine est suffisant et qu'il correspond à celui choisi dans les réglages précédents. Puis sur le panneau de contrôle de l'imprimante, la vitesse à 100%, la hauteur couches  (Z 0.20). Une fois les vérifications faites l'impression débute après le calibrage et la mise à température de la buse et du plateau.

Afin d'être sûr qu'il n'y a pas de problème il est demandé de rester auprès de l'impression au moins pour les premières couches.

![](../images/impressionlaser.png)

## Résultat

Mon objet s'est imprimé en 1h06.
Etant d'une forme simple le corps de l'objet ne présente pas de défauts majeurs. Cependant, à cette échelle le filetage n'a pas fonctionné. De plus, si cet objet devait être fonctionnel, c'est-à-dire étanche, son épaisseur devrait être bien plus importante.

![](../images/objet1.jpg)

_Pour télécharger le fichier .stl [Cliquez ici](../images/gourdesimpleimprante.stl)_
