# 4. Découpe assistée par ordinateur

Cette semaine nous avons été formés à la découpe laser. Dans un premier temps nous avons effectué des tests de calibrages sur les machines. Puis dans un second temps nous devions élaborer un projet de lampe en polypropylène.

## La découpe laser

Deux machines sont disponibles au Fablab : La Lasersaur et L'EpilogFusion

[Lasersaur](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md)

[EpilogFusion](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/EpilogFusion.md)

Afin de préparer une découpe, vous pouvez réaliser un dessin vectoriel en attribuant à vos différentes lignes une couleurs selon le type de découpe souhaité (coupe, plis, gravure) puis l'exporter au format .svg pour être lu par les interfaces des machines. Une fois sur ces interfaces les couleurs accordés à vos lignes doivent être paramétrées. 

Pour choisir ces caractéristiques nous avons réalisés des tests de calibrages sur l'EpilogFusion, un premier [calibrage de découpe](../images/calibration_grid.svg) selon la vitesse et la puissance. Puis un [calibrage de pliage](../images/calibration_grid_pliage.svg) selon une vitesse constante de 75% et une puissance variable.

![](../images/calibrage.png)

## Lampe

Nous avions comme exercice de concevoir une lampe en utilisant une feuille de polypropylène (50x70cm) en cherchant à s'inspirer de notre objet de projet. Le tout par l'utilisation de la découpe laser et sans utiliser ni colle ou vis.

Dans mes premières idées je me focalisais sur les caractéristiques de la gourde, ce qui donnait des réalisations trop complexes. Je me suis donc détachée de mon objet pour chercher la simplification à l'extrême, en utilisant les courbes naturelles de la matière. 

![](../images/1m4.png)

![](../images/2m4.png)

![](../images/3m4.png)

J'ai ensuite réalisé le dessin de découpe sur Illustrator pour ensuite l'exporté en [.svg](../images/lampe.svg) sur l'EpilogFusion. Toutes mes lignes sont des découpes paramétrées en vitesse 10% et puissance 20% c'est pourquoi je n'ai besoin que d'une couleur sur mon fichier CAO.

![](../images/caom4.png)

Le fichier .svg est ensuite envoyé à la découpe depuis l'interface de l'imprimante.

![](../images/4m4.png)

Le résultats aurait besoin d'amélioration, puisque la lumière est beaucoup trop éblouissante. Lors de la conception je souhaitais réaliser cette lampe à une bien plus grande échelle afin qu'elle puisse être posée au sol et soutenir l'ampoule, qui elle aussi devrait être changé pour obtenir une lumière plus douce.
