# 5. Shaper Origin

Cette semaine on a suivi une formation supplémentaire pour appréhender la Shaper Origin, une défonceuse à assitance numérique. Cette machine permet de couvrir une surface de coupe plus importante que la CNC. Elle peut être utiliser à partir de vos propre fichiers .svg sur clé usb.

![](../images/shaperorigin.jpg)

## Précautions

Les précautions d'utilisations les plus importantes sont :

- Vérifier la stabilité de votre plan de travail et pouvoir effectuer la manipulation dans une position stable.
- La fixation du matériaux de découpe au support par du scotch double face ou des serres joints, ce qui empêchera que vos éléments ne se détachent après la coupe.
- Avoir une tenue approprié pour éviter d'être gêné ou qu'un élément vienne se coincer dans la machine.

Les matériaux compatibles avec la machine sont le bois et les métaux non ferreux.

## Réglages

Une fois votre matériau fixé au plan de travail, on peut commencer les réglages sur la machine. On vient changer la fraise selon la précision de coupe. Pour cela, on débranche le mandrin et on dévisse la vis latérale. Une fois la partie mécanique séparé du reste de la machine on peut retirer la mèche en dessérant l'écrou à l'aide d'une clé et du bouton.

Une fois avoir changer la fraise on peut procèder au collage des bandes de Shaper Tape (espacées de 8cm). Les tapes doivent également être positionnés en avant de votre zone de découpe (15cm) pour que la machine puisse les scanner. Lors du scan ces tapes apparaîtrons bleu sur l'écran de la machine.

![](../images/scanbleu.png)

On va ensuite pouvoir placer notre dessin d'une clé usb sur la surface de travail. Puis choisir le type de découpe (sur la ligne, à l'intérieur, à l'extérieur) ainsi que la profondeur de coupe selon l'épaisseur du matériau, et le calibrage de 6 vitesses est disponible. Le z touch permet à la machine d'évaluer la hauteur de la fraise par rapport au matériau et donc l'épaisseur de la découpe.

![](../images/1m5.png)

## Utilisations

Lorsque vous êtes prêt pour le fraisage il ne faut pas oublier de mettre en marche l'aspiration. Sur les poignées le bouton vert permet de commencer la fraisage, le bouton orange d'y mettre fin, alors que le on/off actionne la mèche. Il ne reste plus qu'à suivre le tracé selon le sens de la flèche sans sortir de la cible blanche.

![](../images/bouton.png)
![](../images/fraisage.png)

## Useful links

- [Shaper](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Shaper.md)
